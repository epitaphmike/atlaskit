import { colors } from '@atlaskit/theme';

export default {
  default: {
    background: {
      default: colors.N20,
      focus: colors.N20,
      hover: colors.N40,
    },
    border: {
      default: colors.N30A,
      focus: colors.B100,
      hover: colors.N70,
    },
  },
  selected: {
    background: {
      default: colors.B400,
      focus: colors.B400,
      hover: colors.B500,
    },
    border: {
      default: colors.B400,
      focus: colors.B100,
      hover: colors.B500,
    },
  },
};

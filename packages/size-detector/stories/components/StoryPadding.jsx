import styled from 'styled-components';

export default styled.div`
  padding: 60px;
  height: 100%;
  box-sizing: border-box;
`;

const path = require('path');

module.exports = [
  {
    name: 'AnalyticsDecorator',
    src: path.join(__dirname, '../src/AnalyticsDecorator.js'),
  },
  {
    name: 'AnalyticsListener',
    src: path.join(__dirname, '../src/AnalyticsListener.js'),
  },
];

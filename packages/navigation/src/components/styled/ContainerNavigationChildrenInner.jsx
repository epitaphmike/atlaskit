// @flow
import styled from 'styled-components';

const ContainerNavigationChildrenInner = styled.div`
  flex: 1 1 100%;
`;
ContainerNavigationChildrenInner.displayName = 'ContainerNavigationChildrenInner';
export default ContainerNavigationChildrenInner;

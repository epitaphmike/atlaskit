import { gridSize } from '@atlaskit/theme';

const itemSpacing: number = gridSize() / 2;

export default itemSpacing;

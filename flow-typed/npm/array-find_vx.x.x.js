// flow-typed signature: dcfc8b9c924bf050de663c4d303a8c13
// flow-typed version: <<STUB>>/array-find_v1/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'array-find'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'array-find' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'array-find/find' {
  declare module.exports: any;
}

declare module 'array-find/test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'array-find/find.js' {
  declare module.exports: $Exports<'array-find/find'>;
}
declare module 'array-find/test.js' {
  declare module.exports: $Exports<'array-find/test'>;
}
